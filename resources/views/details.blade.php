@extends('layouts.app')

@section('heading')
    @include('partials.heading')
@endsection

@section('content')
    @include('partials.search-input')
    <div class="container mx-auto w-10/12 lg:w-4/12 my-8">
        <div class="bg-white rounded-lg border border-gray-200 shadow-md">
            <img src="{{ $details['image']['url'] }}" class="w-full rounded-t-lg" alt="{{ $details['name'] }}">
            <div class="p-5">
                @if($details['biography']['publisher'] !== 'null')
                    <span
                        class="mb-5 bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded">{{ $details['biography']['publisher'] }}</span>
                @endif
                <h2 class="mt-5 mb-2 text-4xl font-bold tracking-tight text-gray-900">{{ $details['name'] }}</h2>
                <h3 class="mt-4 mb-2 text-lg font-semibold text-gray-900">Appearance</h3>
                <ul class="space-y-1 list-disc list-none list-inside text-gray-500">
                    <li><strong>Gender</strong>: {{ $details['appearance']['gender'] }}</li>
                    <li><strong>Race</strong>: {{ $details['appearance']['race'] }}</li>
                    <li><strong>Height</strong>: {{ $details['appearance']['height'][1] }}</li>
                    <li><strong>Weight</strong>: {{ $details['appearance']['weight'][1] }}</li>
                    <li><strong>Eye Color</strong>: {{ $details['appearance']['eye-color'] }}</li>
                    <li><strong>Hair Color</strong>: {{ $details['appearance']['hair-color'] }}</li>
                </ul>
                <h3 class="mt-4 mb-2 text-lg font-semibold text-gray-900">Biography</h3>
                <ul class="space-y-1 list-none list-disc list-inside text-gray-500">
                    <li><strong>Full Name</strong>: {{ $details['biography']['full-name'] }}</li>
                    <li><strong>Alter Egos</strong>: {{ $details['biography']['alter-egos'] }}</li>
                    <li><strong>Aliases</strong>: @foreach($details['biography']['aliases'] as $key => $aliases)
                            {{ $aliases . ',' }}
                        @endforeach</li>
                    <li><strong>Place of Birth</strong>: {{ $details['biography']['place-of-birth'] }}</li>
                    <li><strong>First Appearance</strong>: {{ $details['biography']['first-appearance'] }}</li>
                    <li><strong>Alignment</strong>: {{ $details['biography']['alignment'] }}</li>
                </ul>
                <h3 class="mt-4 mb-2 text-lg font-semibold text-gray-900">PowerStats</h3>
                <ul class="space-y-1 list-none list-disc list-inside text-gray-500">
                    <li><strong>Intelligence</strong>: {{ $details['powerstats']['intelligence'] }}%</li>
                    <li><strong>Strength</strong>: {{ $details['powerstats']['strength'] }}%</li>
                    <li><strong>Speed</strong>: {{ $details['powerstats']['speed'] }}%</li>
                    <li><strong>Durability</strong>: {{ $details['powerstats']['durability'] }}%</li>
                    <li><strong>Power</strong>: {{ $details['powerstats']['power'] }}%</li>
                    <li><strong>Combat</strong>: {{ $details['powerstats']['combat'] }}%</li>
                </ul>
                <h3 class="mt-4 mb-2 text-lg font-semibold text-gray-900">Work</h3>
                <ul class="space-y-1 list-none list-disc list-inside text-gray-500">
                    <li><strong>Occupation</strong>: {{ $details['work']['occupation'] }}</li>
                    <li><strong>Base of Operation</strong>: {{ $details['work']['base'] }}</li>
                </ul>
                <h3 class="mt-4 mb-2 text-lg font-semibold text-gray-900">Connections</h3>
                <ul class="space-y-1 list-none list-disc list-inside text-gray-500">
                    <li><strong>Group Affiliation</strong>: {{ $details['connections']['group-affiliation'] }}</li>
                    <li><strong>Relatives</strong>: {{ $details['connections']['relatives'] }}</li>
                </ul>
            </div>
        </div>
        <div class="mt-8">
            <a href="{{ route('index.search') }}"
               class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">Back to Home</a>
        </div>
    </div>
@endsection
