@extends('layouts.app')

@section('heading')
    @include('partials.heading')
@endsection

@section('content')
    @include('partials.search-input')
    @if ($superheroes)
        <div class="container mx-auto w-10/12 lg:w-4/12">
            @foreach ($superheroes as $superhero)
                <a href="details/{{ $superhero['id'] }}"
                   class="flex flex-col items-center bg-white rounded-lg border shadow-md md:flex-row hover:bg-gray-100 my-8">
                    <img src="{{ $superhero['image']['url'] }}"
                         class="object-cover w-full h-96 rounded-t-lg md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                         alt="{{ $superhero['name'] }}">
                    <div class="flex flex-col justify-between p-4 leading-normal">
                        @if($superhero['biography']['publisher'] !== 'null')
                            <span
                                class="mb-5 bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded">{{ $superhero['biography']['publisher'] }}</span>
                        @endif
                        <h2 class="m-0 text-2xl font-bold tracking-tight text-gray-900">{{ $superhero['name'] }}</h2>
                        <p class="mb-3 text-gray-500"><small>{{ $superhero['appearance']['gender'] }}
                                - {{ $superhero['appearance']['race'] }}</small></p>
                        <p class="mb-3 font-normal text-gray-700">
                            Height: {{ $superhero['appearance']['height'][1] }}<br>
                            Weight: {{ $superhero['appearance']['weight'][1] }}</p>
                        @if($superhero['powerstats']['power'] !== 'null')
                            <div class="flex justify-between mb-1">
                                <span class="text-base font-medium text-blue-700">Power</span>
                                <span
                                    class="text-sm font-medium text-blue-700">{{ $superhero['powerstats']['power'] }}%</span>
                            </div>
                            <div class="w-full bg-gray-200 rounded-full h-2.5">
                                <div class="bg-blue-600 h-2.5 rounded-full"
                                     style="width: {{ $superhero['powerstats']['power'] }}%"></div>
                            </div>
                        @endif
                    </div>
                </a>
            @endforeach
            <div class="my-8">
                <a href="{{ route('index.search') }}"
                   class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">Back
                    to Home</a>
            </div>
        </div>
    @else
        <div class="container mx-auto w-4/12 mt-8">
            <div class="row justify-content-center">
                @if ($error_type == 'no results')
                    <div class="flex p-4 mb-4 text-sm text-blue-700 bg-blue-100 rounded-lg" role="alert">
                        <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor"
                             viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                                  clip-rule="evenodd"></path>
                        </svg>
                        <span class="sr-only">Info</span>
                        <div>
                            <span class="font-bold">NO RESULT:</span> no superhero or villain found! Try <a
                                href="{{ route('index.search') }}?search=Bat" class="alert-link">"Bat"</a>.
                        </div>
                    </div>
                @endif
                @if ($error_type == 'api error')
                    <div class="flex p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg" role="alert">
                        <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor"
                             viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                                  clip-rule="evenodd"></path>
                        </svg>
                        <span class="sr-only">Info</span>
                        <div>
                            <span class="font-bold">API ERROR:</span> {{ $api_error }}
                        </div>
                    </div>
                @endif
            </div>
            <div class="my-8">
                <a href="{{ route('index.search') }}"
                   class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">Back
                    to Home</a>
            </div>
        </div>
    @endif
@endsection
