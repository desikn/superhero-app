<div class="container mx-auto w-10/12 lg:w-6/12">
    <h1 class="mb-8 mt-[10vh] text-2xl text-center font-black uppercase text-white md:text-4xl lg:text-6xl lg:leading-[5rem]">
        Search all
        <mark class="px-2 py-0 text-white bg-purple-600 rounded">Superheroes</mark>
        and
        <mark class="px-2 text-white bg-blue-500 rounded">Villains</mark>
        data from all universes under a single API
    </h1>
</div>
