<!doctype html>
<html lang="{{ Str::replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Search all Superheroes and Villains data from all universes under a single API</title>
    <link rel="icon" href="{{ asset('images/favicon.svg') }}" type="image/svg+xml">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    {{--<link rel="stylesheet" href="{{ asset('build/assets/app.b82316ac.css') }}">--}}
</head>
<body
    style="background: url('{{ asset('images/background.jpg') }}') center no-repeat; background-size: cover; background-attachment: fixed;">
<header>
    @yield('heading')
</header>
<main>
    @yield('content')
</main>
{{--<script src="{{ asset('build/assets/app.b7bcc286.js') }}" defer></script>--}}
</body>
</html>
