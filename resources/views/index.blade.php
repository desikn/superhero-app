@extends('layouts.app')

@section('heading')
    @include('partials.heading')
@endsection

@section('content')
    @include('partials.search-input')
@endsection
