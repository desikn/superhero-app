<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DetailController extends Controller
{
    public function details($id)
    {
        $superhero_APIKEY = env('SUPERHERO_APIKEY');
        $details = Http::get("https://www.superheroapi.com/api/" . $superhero_APIKEY . "/" . $id);

        if ($details['response'] == 'success') {
            return view('details', [
                'details' => $details
            ]);
        }

        if ($details['response'] == 'error') {
            if ($details['error'] == 'invalid id') {
                return view('errors.404');
            }
        }
    }
}
