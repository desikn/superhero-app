<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SearchController extends Controller
{
    public function search()
    {
        $search_query = request()->query('search');

        if ($search_query) {
            $superhero_APIKEY = env('SUPERHERO_APIKEY');
            $search = Http::get("https://www.superheroapi.com/api/" . $superhero_APIKEY . "/search/" . $search_query);

            if ($search['response'] == 'success') {
                return view('search', [
                    'search_query' => $search_query,
                    'superheroes' => $search['results']
                ]);
            }
            if ($search['response'] == 'error') {
                if ($search['error'] == 'superhero or villain not found') {
                    return view('search', [
                        'search_query' => $search_query,
                        'superheroes' => false,
                        'error_type' => 'no results'
                    ]);
                } else {
                    return view('search', [
                        'search_query' => $search_query,
                        'superheroes' => false,
                        'error_type' => 'api error',
                        'api_error' => $search['error']
                    ]);
                }
            }
        } else {
            return view('index');
        }
    }
}
